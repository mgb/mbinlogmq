## mbinlogmq 一个基于MySQL binlog协议的数据同步中间件

#### 什么是mbinlogmq?

**mbinlogmq** 是一个使用C语言开发的基于 MySQL **binlog** 协议的一个中间件，通过模拟Slave 来实时获取 MySQL binlog日志，并将数据变更信息以及 **DB** 语句发送到 **RabbitMQ**，中间通过监听机制与校验机制来确保不宕机的情况下的100%抵达 **RMQ**

#### 设计架构



![](mbinlogmq.png)

#### 安装&编译

- mbinlog依赖于一些第三方库：

请先安装依赖软件： `cURL` 、`mysql`、`rabbitmq`，安装完成后记住其安装的路径，进入下面的步骤

- 下载`mbinlogmq`源码

```shell
git clone https://gitee.com/josinli/mbinlogmq.git
```

- 修改 `CMakeLists.txt`，将如下的几行变为您上面安装的路径：

```shell
set(MYSQL_INCLUDE     /usr/local/mysql/include)      # 您系统的MySQL头文件路径
set(MYSQL_LIBRARY_DIR /usr/local/mysql/lib)          # 您系统的MySQL库文件路径
set(CURL_DIR          /usr/local/Cellar/curl/7.58.0) # 您系统的curl安装路径
```

- 执行编译安装命令

```shell
mkdir build
cd build
cmake .. && make && sudo make install	
```

- 修改 `/etc/mbinlogmq/binlog.xml` 您的配置信息

```xml
<?xml version="1.0" encoding="UTF-8" author="Josin" date="2019-07-22"?>
<!--本配置文件安装位置：/etc/mbinlogmq/binlog.xml-->
<!--连接主库进行salve复制的信息-->
<!--请不要增加节点信息，系统会校验-->
<system for="slave">
    <mode>rabbitmq</mode> <!-- 目前仅支持rabbitmq -->
    <host>127.0.0.1</host>
    <user>root</user>
    <password>3333</password>
    <port>3306</port>
    <server_id>6</server_id>
    <daemon>1</daemon>
</system>

<!--用户需要配置好下方的rabbitmq的配置选项-->
<rabbitmq>
    <host>127.0.0.1</host>
    <user>guest</user>
    <password>guest</password>
    <port>15672</port>
    <delivery_mode>1</delivery_mode>
    <name>amq.default</name>
    <payload>Invalid</payload>
    <routing_key>fanout</routing_key>
    <vhost>/</vhost>
</rabbitmq>
```

- 启动您的 **minblogmq**

```shell
mbinlogmq -k start
```

此时进入 **MySQL** 操作，同时登录 **RabbitMQ** 查看消息是否成功抵达，目前的版本存在一些或多或少的问题，切勿生产环境使用。

**mbinlogmq会针对不同的binlog日志生成四种类型的消息，消息类型如下**

- 消息全部为 **JSON** 格式
- 格式如下

1、**UPDATE** 语句，etype等于 1，包含一个`pre`对象 和 `new`对象，分别对应修改之前的数据和新的数据

```json
{
    "etype": 1,
    "data" : {
        "pre": {
            "id":1,
            "ad":2
        },
        "new": {
            "id":1,
            "ad":3
        }
    }
}
```

2、**INSERT** 消息， etype 等于 2

```json
{
    "etype": 1,
    "data" : {
        "id":1,
        "ad":3
    }
}
```

3、**DELETE** 消息， etype 等于 3

```
{
    "etype": 3,
    "data" : {
        "id":1,
        "ad":3
    }
}
```

4、**SQL 语句** 消息， etype 等于 4

```json
{
    etype: 4,
    data: "ALTER TABLE `books`.`test_at` MODIFY COLUMN `cc` datetime(0) NULL DEFAULT NULL AFTER `bb`"
}
```

示例消息如下：

```
 [x] Received {"etype":4,"data":"ALTER TABLE `books`.`test_at` \nMODIFY COLUMN `bb` date NULL DEFAULT NULL AFTER `aa`"}
 [x] Received {"etype":4,"data":"ALTER TABLE `books`.`test_at` \nMODIFY COLUMN `bb` date NOT NULL AFTER `aa`"}
 [x] Received {"etype":4,"data":"BEGIN"}
 [x] Received {"etype":1,"data":{"pre":{"id":1,"book_name":"傲世丹神1","book_cover":"http://img.c0m.io/quanben.io/upload/thumbnail/book_0_999/book_1.jpg","add_time":"2019-7-23 14:28:43","author_name":"寂小贼111","introduction":"废柴少年得无上传承,获逆天神脉,学绝世神功,掌握超绝丹术,这使他...","test_date":"2019-7-29"},"new":{"id":1,"book_name":"傲世丹神1","book_cover":"http://img.c0m.io/quanben.io/upload/thumbnail/book_0_999/book_1.jpg","add_time":"2019-7-23 21:47:36","author_name":"寂小贼","introduction":"废柴少年得无上传承,获逆天神脉,学绝世神功,掌握超绝丹术,这使他...","test_date":"2019-7-29"}}}
 [x] Received {"etype":4,"data":"COMMIT"}
 [x] Received {"etype":4,"data":"BEGIN"}
 [x] Received {"etype":1,"data":{"pre":{"id":1,"book_name":"傲世丹神1","book_cover":"http://img.c0m.io/quanben.io/upload/thumbnail/book_0_999/book_1.jpg","add_time":"2019-7-23 21:47:36","author_name":"寂小贼","introduction":"废柴少年得无上传承,获逆天神脉,学绝世神功,掌握超绝丹术,这使他...","test_date":"2019-7-29"},"new":{"id":1,"book_name":"傲世丹神","book_cover":"http://img.c0m.io/quanben.io/upload/thumbnail/book_0_999/book_1.jpg","add_time":"2019-7-23 21:47:54","author_name":"寂小贼","introduction":"废柴少年得无上传承,获逆天神脉,学绝世神功,掌握超绝丹术,这使他...","test_date":"2019-7-29"}}}
 [x] Received {"etype":4,"data":"COMMIT"}
```



有问题可以及时通过 ISSUE 反馈，反馈地址：[GITEE Go Go Go!!!](https://gitee.com/josinli/mbinlogmq)