/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 */

#ifndef FASTCJSON_FC_CONFIG_H
#define FASTCJSON_FC_CONFIG_H

/**
 * @brief Introduction
 * Common C standard headers
 */
#include <stdio.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <signal.h>
#include <sys/types.h>
#include <errno.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <src/tool/fc_log.h>

#define FC_OFFSET(type, field) ((size_t)&(((type *)0)->field))
#define fc_container_of(p, type, mem) ( (char *)p - FC_OFFSET(type, mem) )

#define PIDFILE "/var/log/mbinlogmq/pidfile"
enum { LOG_INFO = 0, LOG_NOTICE, LOG_WARNING, LOG_ERROR };

#define elif  else if
#define TRUE     1
#define FALSE    0


#if FC_DEBUG
    #define FC_ERROR_EXIT(...) do { _logger(LOG_ERROR, __VA_ARGS__); fprintf(stderr, __VA_ARGS__); exit(0); } while(0)
#else
    #define FC_ERROR_EXIT(...) do { _logger(LOG_ERROR, __VA_ARGS__); } while(0)
#endif


#endif /* FASTCJSON_FC_CONFIG_H */
