/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 */

#ifndef MYSQL_BINLOG_FC_XML_CONFIG_H
#define MYSQL_BINLOG_FC_XML_CONFIG_H

#include <fc_xml.h>

CXML_FIELD_DEF(binlog)
    *system, *rabbitmq
CXML_FIELD_DEF_END(binlog);
CXML_FIELD_FUNC_DEF(binlog);

CXML_FIELD_DEF(system)
    *mode, *host, *user, *password,
    *port, *server_id, *daemon
CXML_FIELD_DEF_END(system);
CXML_FIELD_FUNC_DEF(system);

CXML_FIELD_DEF(rmq)
    *host, *user, *password, *port, *delivery_mode,
    *name, *payload, *routing_key, *vhost
CXML_FIELD_DEF_END(rmq);
CXML_FIELD_FUNC_DEF(rmq);

#endif /* MYSQL_BINLOG_FC_XML_CONFIG_H */
