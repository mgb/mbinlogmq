/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 */

#include <fc_xml_config.h>

/**
 * @brief NOTICE
 * For the System XML config file.
 */
CXML_FIELD_FUNC(binlog)
    CXML_FIELD_CMP(system,   system,    6,   if)
    CXML_FIELD_CMP(rabbitmq, rabbitmq,  8, elif)
CXML_FIELD_FUNC_END();


/**
 * @brief NOTICE
 * system node
 */
CXML_FIELD_FUNC(system)
    CXML_FIELD_CMP(mode,     mode,       4,   if)
    CXML_FIELD_CMP(host,     host,       4, elif)
    CXML_FIELD_CMP(user,     user,       4, elif)
    CXML_FIELD_CMP(port,     port,       4, elif)
    CXML_FIELD_CMP(password, password,   8, elif)
    CXML_FIELD_CMP(server_id,server_id,  9, elif)
    CXML_FIELD_CMP(daemon,   daemon,     6, elif)
CXML_FIELD_FUNC_END();


/**
 * @brief NOTICE
 * RabbitMQ node
 */
CXML_FIELD_FUNC(rmq)
    CXML_FIELD_CMP(host,     host,       4,   if)
    CXML_FIELD_CMP(user,     user,       4, elif)
    CXML_FIELD_CMP(port,     port,       4, elif)
    CXML_FIELD_CMP(port,     port,       4, elif)
    CXML_FIELD_CMP(name,     name,       4, elif)
    CXML_FIELD_CMP(vhost,    vhost,      5, elif)
    CXML_FIELD_CMP(payload,  payload,         7, elif)
    CXML_FIELD_CMP(password,      password,   8, elif)
    CXML_FIELD_CMP(delivery_mode, delivery_mode, 13, elif)
    CXML_FIELD_CMP(routing_key,   routing_key,   11, elif)
CXML_FIELD_FUNC_END();