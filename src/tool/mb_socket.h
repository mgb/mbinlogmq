/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 */

#ifndef MYSQL_BINLOG_MB_SOCKET_H
#define MYSQL_BINLOG_MB_SOCKET_H

#include <fc_config.h>
#include <fc_string.h>

ssize_t mb_socket_read(int fd, void *ptr, size_t plen);
ssize_t mb_socket_read_str_len(int fd, char **ptr, size_t plen);
CSTRING *mb_socket_read_stream(int fd);

#endif /* MYSQL_BINLOG_MB_SOCKET_H */
